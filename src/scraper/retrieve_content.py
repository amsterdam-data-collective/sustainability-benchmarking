#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for scraping and parsing HTML pages

import re
from util import *
from bs4 import BeautifulSoup

def get_urls(brand, href):
    '''Function which creates a list of urls for given brand

    Parameters
    ----------
    brand : str
        Brandname
    href : str
        Homepage of brand

    Returns
    -------
    found_urls
        a list of urls
    '''

    print('Searching URLs for {}..'.format(brand))

    if not href or href == 'None':
        print('No homepage found for {}..'.format(brand))
        return

    # first, retrieve all hrefs found on homepage
    found_urls = find_hrefs(href)

    # then, open all hrefs and retrieve hrefs again
    sub_urls = []
    for url in found_urls:
        sub_url = find_hrefs(url)
        sub_urls.append(sub_url)

    found_urls += [y for x in sub_urls for y in x]
    found_urls = list(set(found_urls))
    print('Found {} URLs for brand: {}'.format(str(len(found_urls)), brand))

    return found_urls


def find_hrefs(href):
    '''Finds all hrefs on given page

    Parameters
    ----------
    href : str
        Hyperlink

    Returns
    -------
    list
        a list of unique urls found on given page
    '''

    try:
        url_response = get_response(href)
        soup = BeautifulSoup(url_response.content, 'html.parser')
    except AttributeError:
        return []
    if not soup:
        return []

    list_of_urls = []
    for idx, tag in enumerate(soup.find_all(href=True)):
        if ignore_pages(tag['href']):
            continue
        try:
            found_url = fix_url(href, tag['href'])
            found_url_domain = urlparse.urlparse(found_url).netloc.strip('www.')
            href_domain = urlparse.urlparse(href).netloc.strip('www.')
            if found_url_domain.split('.')[0] != href_domain.split('.')[0]:
                continue
            list_of_urls.append(found_url)
        except Exception as e:
            print('Error in function find_hrefs: {}'.format(e))
            continue
    return list(set(list_of_urls))


def scrape_urls(brandname, imp_urls, next_idx):
    '''Scapes all found urls per brand

    Parameters
    ----------
    brandname : str
        Brandname
    imp_urls : list
        List containing all found urls

    Returns
    -------
    imp_url_ids
        a list of ids which link to urls in html_dict
    html_dict
        a dictionary containing urls and its corresponding html (content)
    next_idx
        a integer representing the next id for a page in the html_dict
    '''

    print('Scraping all URLs for {}..'.format(brandname))

    # url_ID = 0
    html_dict = dict()
    imp_url_ids = []
    if not brandname or not imp_urls:
        return None, None, next_idx
    elif brandname == 'None' or imp_urls == 'None':
        return None, None, next_idx

    for idx, path in enumerate(imp_urls):

        try:
            url_response = get_response(path)
            url_html = BeautifulSoup(url_response.content, 'html.parser')
            url_content = parse_html(url_html)
            url_content_EN = translate_to_EN(url_content)
        except Exception as e:
            print(e)
            continue

        # make new html dict with count as url ID, append ID to brands_dict
        html_dict[next_idx] = {'brand_name': brandname, 'url': path, 'html_code': url_html.prettify(), 'html_content': url_content, 'html_content_EN': url_content_EN}
        imp_url_ids.append(next_idx)
        next_idx += 1

    return imp_url_ids, html_dict, next_idx


def parse_html(soup):
    '''Search HTML for paragraphs and headers

    Parameters
    ----------
    soup : BeautifulSoup object
        Object containing HTML code

    Returns
    -------
    str
        a string containing headers and paragraphs found in HTML code
    '''

    paragraphs = ''
    p_result = soup.find_all('p')
    for p in p_result:
        p_text = p.get_text().strip()
        if p_text and p_text[0].isalpha():
            paragraphs += ' '.join(p_text.split()) + ' '

    headers = ''
    h_result = soup.find_all(re.compile('^h[1-6]$'))
    for h in h_result:
        h_text = h.get_text().strip()
        if h_text and h_text[0].isalpha():
            headers += ' '.join(h.get_text().split()) + ' '

    return '%s %s' % (headers, paragraphs)
