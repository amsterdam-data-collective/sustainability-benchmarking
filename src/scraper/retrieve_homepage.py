#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for retrieving homepage of brand

import re
from util import *
from googlesearch import search

def get_homepage(brand):
    '''Retrieves homepage of brand using Google search

    Parameters
    ----------
    brand : str
        Brandname

    Returns
    -------
    correct_page
        String containing the homepage of given brand
    None
        None if no homepage is found
    '''

    print('Retrieving homepage for {}..'.format(brand))

    query = brand + ' clothing'
    correct_page = None
    try:
        for page in search(query, tld='com', lang='en', num=10, stop=6, pause=3):
            parsed_url = urlparse.urlparse(page)
            clean_url = parsed_url.scheme + '://' + parsed_url.netloc
            clean_brand = re.sub('[^a-zA-Z0-9]', '', brand)
            # check if brandname is in url
            if clean_brand in parsed_url.netloc:
                print('succes')
                correct_page = clean_url
                break

    except Exception as e:
        print(e)
        return correct_page

    return correct_page
