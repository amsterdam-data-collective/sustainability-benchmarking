#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for exporting sql table of brands

import sqlite3


def save_to_sql(brands_dict, html_dict, db_path, new = False):
    '''Saves dictionaries in a SQL database with two tables
    Example found on https://www.python-course.eu/sql_python.php

    Parameters
    ----------
    brands_dict : dict
        Dictionary containing brands
    html_dict : dict
        Dictionary containing urls and corresponding html (content)
    db_path : str
        Path to new database file
    new : bool
        Flag, if True then a new database is created, inserts to existing table by default
    '''

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    if new:
        print('\nCreating new SQL database..')
        sql_command = """
                        CREATE TABLE brands (
                        id INTEGER PRIMARY KEY,
                        brand_name VARCHAR(100),
                        url VARCHAR(10000),
                        all_urls VARCHAR(10000000),
                        n_urls INTEGER(10),
                        imp_urls VARCHAR(10000000),
                        imp_urls_ID VARCHAR(10000),
                        label_gold VARCHAR(10),
                        vectors VARCHAR(10000000),
                        predict_class VARCHAR(10),
                        predict_proba VARCHAR(100),
                        predict_neighbour VARCHAR(10),
                        predict_closeclass VARCHAR(10),
                        predict_n_std VARCHAR(100),
                        post_prediction VARCHAR(10)
        );"""
        cursor.execute(sql_command)

        sql_command = """
                        CREATE TABLE html (
                        id INTEGER PRIMARY KEY,
                        brand_name VARCHAR(100),
                        url VARCHAR(10000),
                        html_code VARCHAR(100000000),
                        html_content VARCHAR(100000000),
                        html_content_EN VARCHAR(100000000));"""
        cursor.execute(sql_command)


    print('Inserting {} brands into brands table..'.format(len(brands_dict)))

    for brand in brands_dict:
        list_of_values = [
            'url',
            'all_urls',
            'n_urls',
            'imp_urls',
            'imp_urls_ID',
            'label_gold',
            'vectors',
            'predict_class',
            'predict_proba',
            'predict_neighbour',
            'predict_closeclass',
            'predict_n_std',
            'post_prediction'
        ]
        for value in list_of_values:
            if value not in brands_dict[brand]:
                brands_dict[brand][value] = ''


        cursor.execute(
            """INSERT INTO brands ("id", "brand_name", "url", "all_urls", "n_urls", "imp_urls", "imp_urls_ID", "label_gold", "vectors", "predict_class", "predict_proba", "predict_neighbour", "predict_closeclass", "predict_n_std", "post_prediction") VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);""",
            (
                brand,
                brands_dict[brand]['url'],
                str(brands_dict[brand]['all_urls']),
                brands_dict[brand]['n_urls'],
                str(brands_dict[brand]['imp_urls']),
                str(brands_dict[brand]['imp_urls_ID']),
                brands_dict[brand]['label_gold'],
                str(brands_dict[brand]['vectors']),
                brands_dict[brand]['predict_class'],
                str(brands_dict[brand]['predict_proba']),
                str(brands_dict[brand]['predict_neighbour']),
                str(brands_dict[brand]['predict_closeclass']),
                str(brands_dict[brand]['predict_n_std']),
                str(brands_dict[brand]['post_prediction'])
            )
        )

    print('Done!')

    print('Inserting {} pages into html table..'.format(len(html_dict)))
    for id in html_dict:
        if 'html_content_EN' not in html_dict[id]:
            html_dict[id]['html_content_EN'] = html_dict[id]['html_content']


        cursor.execute(
            """INSERT INTO html ("id", "brand_name", "url", "html_code", "html_content", "html_content_EN") VALUES (?, ?, ?, ?, ?, ?);""",
            (id, html_dict[id]['brand_name'], html_dict[id]['url'], html_dict[id]['html_code'], html_dict[id]['html_content'], html_dict[id]['html_content_EN']))
    print('Done!')

    print('Saving database..')

    connection.commit()
    connection.close()

    print('Done!')
