#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# utility script

import re
import requests
import translators as ts
import time

from export_sql import save_to_sql
from bs4 import BeautifulSoup
try:
    import urlparse
except ImportError:
    import urllib.parse as urlparse

# global variables for proxy handling
PROXY_LIST_URL = 'https://free-proxy-list.net/'
PROXY = {}
AVAILABLE_PROXIES =[]

# these headers make it seem as if a webpage is approached by a human
HEADERS = {
    'Accept-Encoding': 'gzip, deflate, sdch',
    'Accept-Language': 'en-US,en;q=0.8',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive',
}



def get_response(href):
    ''' Function for getting response of webpage

    Parameters
    ----------
    href : str
        URL in string format

    Returns
    -------
    r
        a requests response
    None
        returns Nonetype if there is no response or an error
    '''

    tries = 3
    while tries > 0:
        # ignore pages .js / cookie / css etc
        if ignore_pages(href):
            return None

        tries -= 1
        try:
            r = requests.get(href, headers = HEADERS, proxies = PROXY, timeout = 3)
        except requests.exceptions.Timeout:
            continue
        except requests.exceptions.TooManyRedirects:
            # print('Too Many Redirects')
            return
        except requests.exceptions.RequestException as e:
            set_next_proxy()
            continue
        except Exception as e:
            continue

        if r.status_code == 200:  # Correct response code
            return r
        else:
            print('Url {} responds with status code {}'.format(href, r.status_code))
            return
    print('Failed to retrieve {}..'.format(href))
    return



def load_proxies():
    ''' Function for retrieving a list of proxies from https://free-proxy-list.net/ '''

    r = get_response(PROXY_LIST_URL)
    soup = BeautifulSoup(r.content, 'html.parser')
    result = soup.find('table', { 'id' : 'proxylisttable'})
    for row in result.findAll('tr'):
        cells = row.findAll('td')
        if len(cells) == 8:
            http = 'https://' if cells[6].find(text=True) == 'yes' else 'http://'
            AVAILABLE_PROXIES.append(http + cells[0].find(text=True))
    set_next_proxy()


def set_next_proxy():
    ''' Function for selecting a proxy, and calling for more proxies if list is empty '''

    if len(AVAILABLE_PROXIES) == 0:
        load_proxies()
    proxy_url = AVAILABLE_PROXIES.pop()
    print('Setting proxy url: {}'.format(proxy_url))
    PROXY = dict(http=proxy_url, https=proxy_url)


def ignore_pages(href):
    ''' Function for ignoring specific file extensions

    Parameters
    ----------
    href : str
        URL in string format

    Returns
    -------
    bool
        returns True if page should be ignored, else False
    '''

    if href.endswith(('.png', '.jpg', '.jpeg', '.pdf', '.bmp', '.gif', '.ico', 'js')):
        return True
    exceptions = ['cookie', 'css', 'javascript', 'mailto']
    for e in exceptions:
        if e in href:
            return True
    return False


def fix_url(domain, path):
    ''' Function for fixing a broken url

    Parameters
    ----------
    domain : str
        domain of URL
    path : str
        path of URL

    Returns
    -------
    fixed_url
        a string containing the fixed url
    '''

    if path.startswith(('http', 'www')):
        url = path
    elif path.startswith('//'):
        url = 'https://' + path.lstrip('//')
    elif domain.endswith('/') and path.startswith('/'):
        url = domain.rstrip('/') + path
    else:
        url = domain + path

    http = url.split('//')[0] + '//'
    domain = ''.join(url.split('//')[1:])
    domain = re.sub('www.', '', domain)

    parts = domain.split('/')
    new_parts = []
    for part in parts[1:]:
        if part not in new_parts:
            new_parts.append(part)
        elif part in url:
            # if double part in url, create new parts list
            new_parts = []
            new_parts.append(part)

    new_parts_url = '/'.join(new_parts)
    fixed_url = http + 'www.' + parts[0] + '/' + new_parts_url

    return fixed_url


def translate_to_EN(content):
    ''' Translate content to English

    Parameters
    ----------
    content : string
        String containing content of an url

    Returns
    -------
    str
        a string representing the English translation
    '''

    try:
        lang = detect(content)
    except Exception as e:
        return content

    # bn causes errors
    if lang in ['en', 'bn']:
        return content

    tries = 3
    while tries > 0:
        tries -= 1
        try:
            # this library has a limit of 5000 characters
            content_EN = ts.google(content[:4999], lang, 'en', proxies=PROXY)
            return content_EN
        except NetworkRequestError:
            continue
        except Exception as e:
            # print(e)
            set_next_proxy()
            continue

    return content


def early_stopping(brandname, brand_url, tmp_brands_dict, current_db, NEW_DB):
    '''Function which creates a smaller dictionary entry if no information is found

    Parameters
    ----------
    brandname : str
        Brandname
    brand_url : str
        Homepage of brand
    tmp_brands_dict : dict
        Dictionary containing brands and their information,
        which will later be added to the current database
    current_db : str
        Current sql database

    Returns
    -------
    tmp_brands_dict
        Temporary dictionary containing the new brand with or without homepage
    '''

    print('Could not find homepage for {}, appending just the brandname to database..'.format(brandname))
    if brand_url:
        tmp_brands_dict[brandname] = {'url': brand_url}
    else:
        tmp_brands_dict[brandname] = {}

    if not NEW_DB:
        save_to_sql(tmp_brands_dict, {}, current_db)
    return tmp_brands_dict, {}
