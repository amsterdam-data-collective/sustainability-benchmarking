#!/usr/bin/python3
# Leon Graumans
# 30-12-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for scraping, using user input


from retrieve_homepage import get_homepage
from retrieve_content import get_urls, scrape_urls
from url_picker import pick_urls
from export_sql import save_to_sql
from util import early_stopping


def start_scraping(brand, url, next_idx, NEW_DB):
    '''Start scraping process for brand and its homepage (url)

    Parameters
    ----------
    brand : str
        brand name in string format
    url : str
        url in string format
    next_idx : int
        next index for html dictionary
    NEW_DB : bool
        flag which determines wether to create a new SQL table


    Returns
    -------
    tmp_brands_dict : dict
        emporary dictionary containing new brand
    tmp_html_dict : dict
        temporary dictionary containing IDs and corresponding html pages
    '''

    tmp_brands_dict = dict()

    # if brand homepage not provided, search using google library
    if not url:
        url = get_homepage(brand)

    # if no homepage is found, add brand only
    if not url:
        tmp_brands_dict, tmp_html_dict = early_stopping(brand, url, tmp_brands_dict, current_db, NEW_DB)
        exit()

    else:
        # retrieve urls found on homepage, and found on each of these pages
        all_urls = get_urls(brand, url)

        # pick 10 urls and scrape
        imp_urls = pick_urls(all_urls)

        imp_url_ids, tmp_html_dict, next_idx = scrape_urls(brand, imp_urls, next_idx)

        if imp_url_ids:
            tmp_brands_dict[brand] = {
                'url': url,
                'all_urls': all_urls,
                'n_urls': len(all_urls),
                'imp_urls': imp_urls,
                'imp_urls_ID': imp_url_ids,
                'label_gold': '',
                'vectors': '',
                'predict_class': '',
                'predict_proba': '',
                'predict_neighbour': '',
                'predict_closeclass': '',
                'predict_n_std': '',
                'post_prediction': ''
            }

        else:
            tmp_brands_dict, tmp_html_dict = early_stopping(brand, url, tmp_brands_dict, current_db, NEW_DB)
            exit()

    return tmp_brands_dict, tmp_html_dict
