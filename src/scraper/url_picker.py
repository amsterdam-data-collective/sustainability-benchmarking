#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for picking the 10 most important urls per brand

import re
import csv
import json
import random
from util import *


def pick_urls(urls, return_productpages = False, ietf_lang = 'scraper/databases/ietf-language-tags_csv.csv', page_dict = 'scraper/databases/page_dictionary.txt'):
    '''Selects 10 important URLs for given brand. Uses keywords to do so. If less
    than 10 URLs are found, the remainder will be randomly picked from a collection
    of URLs (excluding productpages and accountpages)

    Parameters
    ----------
    urls : list
        List of URLs for given brand
    return_productpages : bool
        Flag, if True, productpages are returned instead of aboutpages, this is
        used for retrieving prices on productpages, False by default

    Returns
    -------
    imp_urls
        a list of 10 (or less) URLs
    productpage_urls
        a list of URLs of productpages
    '''

    print('Selecting the best URLs per brand..')

    # import csv with ietf language tags
    lang_tags = dict()
    with open(ietf_lang) as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            lang_tags[line[0].lower()] = line[1]

    # import json file containing lists of keywords
    with open(page_dict) as json_file:
        pages = json.load(json_file)

    # if list of urls is saved as string, then convert to list
    if isinstance(urls, str):
        urls = urls.strip('[]').split(',')

    list_of_paths = []
    for url in urls:
        parsed_url = urlparse.urlparse(url)
        if parsed_url.path.lower().startswith(('/wp-json', '/../..', '\\', '/mailt')):
            continue
        if parsed_url.path.lower().endswith(('.xml', '.oembed', '.atom', '.js', 'json', '.svg')):
            continue

        clean_url = parsed_url.scheme + '://' + parsed_url.netloc + parsed_url.path
        url_parts = ['/' + part for part in parsed_url.path.split('/') if part]
        if not url_parts:
            continue

        # often, urls contain two language tags: /en/en-gb/
        # if so, delete these tags from the url_parts
        # and save first part of path & full path in list_of_paths
        tmp = re.sub('_', '-', url_parts[0].lstrip('/'))
        if tmp in lang_tags:
            if lang_tags[tmp] != 'en':
                continue
            url_parts = url_parts[1:] # delete item 0
            if not url_parts:
                continue
            tmp = re.sub('_', '-', url_parts[0].lstrip('/'))
            if tmp in lang_tags:
                if lang_tags[tmp] != 'en':
                    continue
                url_parts = url_parts[1:] # delete item 0
        if url_parts:
            list_of_paths.append((url_parts, clean_url))

    # create list of all_urls, except for product & accountpages
    # create list of urls that match any of the keywords in aboutpages
    all_urls = []
    imp_urls = []
    productpage_urls = []
    for path in list_of_paths:
        if any(sub_url in path[0] for sub_url in pages['product']):
            productpage_urls.append(path[1])
            continue
        elif any(sub_url in path[0] for sub_url in pages['account']):
            continue
        all_urls.append(path[1])
        if any(sub_url in path[1] for sub_url in pages['about']):
            imp_urls.append(path[1])

    all_urls = list(set(all_urls))
    imp_urls = list(set(imp_urls))
    productpage_urls = list(set(productpage_urls))

    # if length of imp_urls is smaller than 10, pick random from all_urls
    # elif length of imp_urls is bigger than 10, random remove some urls
    if len(imp_urls) < 10:
        remainder = 10 - len(imp_urls)
        random.shuffle(all_urls)
        part_urls = all_urls[:remainder]
        imp_urls += part_urls
    elif len(imp_urls) > 10:
        random.shuffle(imp_urls)
        imp_urls = imp_urls[:10]

    if not return_productpages:
        return imp_urls
    else:
        return imp_urls, productpage_urls
