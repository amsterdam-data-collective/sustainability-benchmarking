#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for loading the data for classification

import sqlite3
import pandas as pd
import numpy as np
import ast
import sys
import re

from ast import literal_eval
from labels import get_label

def load_brands_db(file):
    '''Loads the brands table from a sql database as dictionary
    Automatically detects how many columns to append to dictionary

    Parameters
    ----------
    file : sql file (.db)
        File containing the brands table

    Returns
    -------
    brands_dict
        a dictionary of brands, labels, homepages, urls etc.
    '''

    connection = sqlite3.connect(file)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM brands")

    column_names = [column[0] for column in cursor.description]
    column_names.pop(1)

    result = cursor.fetchall()
    brands_dict = dict()
    for row in result:
        brands_dict[row[1]] = {}
        col_idx = 0
        for row_idx in range(len(row)):
            if row_idx == 1:
                continue
            brands_dict[row[1]][column_names[col_idx]] = row[row_idx]
            col_idx += 1

    print('Done, loaded {} brands in json format!'.format(len(brands_dict)))

    return brands_dict


def load_html_db(file):
    '''Loads the html table from a sql database as dictionary

    Parameters
    ----------
    file : sql file (.db)
        File containing the html table

    Returns
    -------
    html_dict
        a dictionary of urls and their corresponding html code
    '''

    connection = sqlite3.connect(file)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM html")

    column_names = [column[0] for column in cursor.description]

    result = cursor.fetchall()
    html_dict = dict()
    for row in result:
        html_dict[row[0]] = {}
        col_idx = 0
        for row_idx in range(len(row)):
            html_dict[row[0]][column_names[col_idx]] = row[row_idx]
            col_idx += 1

    print('Done, loaded {} URLs in json format!'.format(len(html_dict)))

    return html_dict


def merge_data(brands_dict, html_dict, skiplabel = True):
    '''Merges brands and html dictionaries

    Parameters
    ----------
    brands_dict : dict
        dictionary of brands
    html_dict : dict
        dictionary of urls and their corresponding html code

    Returns
    -------
    X
        a pandas dataframe with columns for brands, content and the amount of
        urls per brand
    y
        a pandas dataframe with columns for labels
    '''

    final_dict = {'brand': [], 'content': [], 'n_url': [], 'vectors': [], 'label': []}
    for brand, values in brands_dict.items():

        label = get_label(values)

        # skip brands without label, for now
        if skiplabel and not label:
            continue

        column = 'imp_urls_ID'
        if column not in values:
            column = 'all_urls_ID'
        url_IDs = values[column]
        # if list of urls is saved as string, then convert to list
        if isinstance(url_IDs, str):
            try:
                url_IDs = ast.literal_eval(url_IDs)
            except:
                url_IDs = re.findall(r'"\s*([^"]*?)\s*"', url_IDs)

        if not url_IDs:
            continue

        content = ''
        for id in url_IDs:
            if id == 'None':
                continue
            if id:
                column = 'html_content_EN'
                if column not in html_dict[int(id)].keys():
                    column = 'html_content'
                text = html_dict[int(id)][column]

                if text:
                    content += text + ' '

        # skip brands without content, for now
        if not content.strip():
            continue

        if label and 'vectors' in values:
            vec = values['vectors']
            if vec:
                vec = literal_eval(vec)
            else:
                vec = []
        else:
            vec = []

        final_dict['vectors'].append(vec)
        final_dict['brand'].append(brand)
        final_dict['label'].append(label)
        final_dict['content'].append(content)

        if isinstance(url_IDs, str):
            n_url = len(values['all_urls'].strip('[]').split(','))
        else:
            n_url = len(values['all_urls'])
        final_dict['n_url'].append(n_url)


    df = pd.DataFrame(final_dict)
    X = df.loc[:, ['brand', 'content', 'n_url', 'label', 'vectors']]
    y = df.loc[:, 'label']

    return X, y, final_dict



def load_embeddings(embedding_file):
    '''Loads embeddings from file

    Parameters
    ----------
    embedding_file : txt
        embeddings stored as txt

    Returns
    -------
    model
        embeddings in a dict-like structure available for look-up
    '''

    print('Using embeddings: ', embedding_file)

    model = {}
    f = open(embedding_file,'r')
    for idx, line in enumerate(f):
        values = line.split()

        # this works for the 300d GloVe embeddings, to ignore faulty rows
        if len(values) != 301:
            continue

        word = values[0]
        try:
            embedding = np.asarray(values[1:], dtype='float')
        except ValueError:
            continue

        model[word] = embedding

    print('Done.',len(model),' words loaded!')
    return model
