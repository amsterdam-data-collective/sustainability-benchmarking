#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for checking certainty of model prediction using word2vec vectors

import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import gensim
from gensim.models import Word2Vec

# The embeddings can be downloaded from command prompt:
# wget -c "https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz"
PATH_TO_WORD2VEC = 'classifier/GoogleNews-vectors-negative300.bin.gz'
MODEL = {}

def calculate_matrices(X):
    '''Calculates matrices for SUS and NOT classes in training data

    Parameters
    ----------
    X : pd DataFrame
        dataframe of train data

    Returns
    -------
    avg_sus_matrix
        average matrix for SUS
    avg_not_matrix
        average matrix for NOT
    sus_matrices
        all matrices for SUS
    not_matrices
        all matrices for NOT
    sus_std
        standard deviation for SUS
    not_std
        standard deviation for NOT
    '''

    print('Calculating matrices for X..')

    sus_matrices = []
    not_matrices = []
    for idx, row in X.iterrows():
        brand = row['brand']
        label = row['label']
        matrix = np.array(row['vectors'])

        try:
            l_matrix = len(matrix)

            if label == 'SUS':
                sus_matrices.append(matrix)
            elif label == 'NOT':
                not_matrices.append(matrix)
        except Exception as e:
            print('error for {}: {}'.format(brand, e))
            empty_matrix = np.zeros((100, 300))
            if label == 'SUS':
                sus_matrices.append(empty_matrix)
            elif label == 'NOT':
                not_matrices.append(empty_matrix)
            continue

    avg_sus_matrix = sum(np.array(sus_matrices)) / float(len(sus_matrices))
    avg_not_matrix = sum(np.array(not_matrices)) / float(len(not_matrices))

    # standard deviation
    sus_std = np.std(sus_matrices)
    not_std = np.std(not_matrices)

    return avg_sus_matrix, avg_not_matrix, sus_matrices, not_matrices, sus_std, not_std


def get_top_n_words(corpus, n=None):
    '''Gets most frequent words using sklearn CountVectorizer, source: https://bit.ly/30vFfjn

    Parameters
    ----------
    corpus : list of strings
        list containing content
    n : bool
        flag to determine the max amount of returned words

    Returns
    -------
    words_freq
        list of most frequent words and their frequency
    '''

    exceptions = [
        'newsletter',
        'subscribe',
        'blog',
        'products',
        'press',
        'page',
        'new',
        'login',
        'account',
        'cart',
        'javascript',
        'disabled',
        'enabled',
        'browser',
        'cookie',
        'cookies'
    ]

    corpus = list(np.hstack(corpus))
    vec = CountVectorizer().fit(corpus)
    bow = vec.transform(corpus)
    sum_words = bow.sum(axis = 0)
    words_freq = [(word, sum_words[0, idx]) for word, idx in vec.vocabulary_.items()]
    words_freq =sorted(words_freq, key = lambda x: x[1], reverse = True)
    words_freq = [wf for wf in words_freq if wf[0] not in exceptions]
    return words_freq[:n]


def get_matrix(content):
    '''returns the vectors for the 100 most frequent words in content

    Parameters
    ----------
    content : str
        string containing brand content

    Returns
    -------
    matrix
        a list of lists (vectors) representing the 100 most frequent words
    '''

    if not MODEL:
        load_model()

    all_stopwords = stopwords.words('english') + stopwords.words('german') + stopwords.words('dutch')
    all_stopwords = list(set(all_stopwords))
    ignore_keywords = ['javascript', 'browser', 'compatible', 'account', 'cookie', 'cookies', 'policy', 'privacy', 'terms', 'disclaimer', 'warranty', 'ship', 'shipping', 'return', 'returned', 'exchange', 'exchanged', 'please']
    ignore_keywords = ignore_keywords + all_stopwords

    most_freq_words = get_top_n_words([content], n = 500)
    most_freq_words = [word[0].lower() for word in most_freq_words if word[0].lower() not in ignore_keywords]
    matrix = [MODEL[word] for word in most_freq_words if word in MODEL][:100]

    return matrix


def load_model():
    '''Loads Word2Vec model'''

    print('Loading model..')
    MODEL = gensim.models.KeyedVectors.load_word2vec_format(PATH_TO_WORD2VEC, binary=True)
    print('Model loaded with a vocab of {}!'.format(len(MODEL.wv.vocab)))


def check_neighbours(X, X_test):
    '''Checks neighbour brands and the distance to the average matrices of
    the SUS and NOT classes

    Parameters
    ----------
    X : pd DataFrame
        dataframe of train data
    X_test : pd DataFrame
        dataframe of test data

    Returns
    -------
    X_test
        dataframe of test data, with new columns (neighbour, closeclass, n_std)

    '''

    avg_sus_matrix, avg_not_matrix, sus_matrices, not_matrices, sus_std, not_std = calculate_matrices(X)

    ''' check distance to class_avg and closest neighbour '''

    print('Checking neighbours and closest classes..')

    close_classes = []
    neighbours = []
    std_list = []
    for idx, row in X_test.iterrows():
        brand = row['brand']
        guess = row['predict_class']
        matrix = row['vectors']
        l_matrix = len(matrix)

        # get matrix
        if l_matrix == 0:
            matrix = get_matrix(row['content'])

        # fill matrix with zeroes if matrix is too small
        if l_matrix < 100:
            matrix = np.zeros((100, 300))
            for idx, line in enumerate(matrix):
                matrix[idx] = line

        # calculate distances to avg class matrix
        sus_dist = np.abs(np.sum(avg_sus_matrix - matrix))
        not_dist = np.abs(np.sum(avg_not_matrix - matrix))

        if sus_dist < not_dist:
            close_classes.append('SUS')
        else:
            close_classes.append('NOT')

        # check for each matrix in sus/not-matrices which distance is lowest
        closest_dist = 1000
        closest_neighbour = ''
        for sus_m in sus_matrices:
            dist = np.abs(np.sum(matrix - sus_m))
            if dist < closest_dist:
                closest_dist = dist
                closest_neighbour = 'SUS'

        for not_m in not_matrices:
            dist = np.abs(np.sum(matrix - not_m))
            if dist < closest_dist:
                closest_dist = dist
                closest_neighbour = 'NOT'

        neighbours.append(closest_neighbour)

        # calculate n times standard deviation
        if guess == 'SUS':
            distance = sus_dist
            std_dev = sus_std
        else:
            distance = not_dist
            std_dev = not_std
        n_std = distance / std_dev
        std_list.append(n_std)

    X_test['predict_closeclass'] = close_classes
    X_test['predict_neighbour'] = neighbours
    X_test['predict_n_std'] = std_list

    return X_test


def make_decision(X, X_test):
    '''Makes decision whether to post or not post prediction

    Parameters
    ----------
    X : pd DataFrame
        dataframe of train data
    X_test : pd DataFrame
        dataframe of test data

    Returns
    -------
    X_test
        dataframe of test data, with new columns (post_prediction etc.)
    '''

    X_test = check_neighbours(X, X_test)

    post_predictions = []
    for _, row in X_test.iterrows():
        predict_class = row['predict_class']
        predict_proba = row['predict_proba']
        predict_neighbour = row['predict_neighbour']
        predict_closeclass = row['predict_closeclass']
        predict_n_std = row['predict_n_std']

        post_decision = False

        if predict_class == 'SUS':
            if predict_proba >= 0.9:
                post_decision = True
            elif predict_proba >= 0.55:
                if predict_class == predict_neighbour or predict_class == predict_closeclass:
                    post_decision = True
        elif predict_class == 'NOT':
            if predict_proba <= 0.1:
                post_decision = True
            elif predict_proba < 0.55:
                if predict_class == predict_neighbour or predict_class == predict_closeclass:
                    post_decision = True

        if post_decision:
            post_predictions.append(1)
        else:
            post_predictions.append(0)

    X_test['post_prediction'] = post_predictions

    print('Done!')

    return X_test
