#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for classification

from features import *
from check import *
from run_gridsearch import perform_gridsearch
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC, SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import FunctionTransformer
from sklearn.metrics import f1_score, accuracy_score
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import precision_recall_curve
from joblib import dump, load
# from lightgbm import LGBMClassifier

import warnings
warnings.filterwarnings('ignore')



def train_test_analyse(X_train, y_train, X_test, y_test, embeddings, SEED, modelname = 'SVC', gridsearch = False):
    '''Classifier with a pipeline containing multiple features
    Fits classifier, predicts label, measures and returns scores

    Parameters
    ----------
    X_train : pd dataframe
        training data
    y_train : pd dataframe
        training labels
    X_test : pd dataframe
        test data
    y_test : pd dataframe
        test labels
    embeddings : dict
        embeddings in a dict-like structure available for look-up
    SEED : int
        number to generate deterministic random data
    Modelname : str
        string for selecting which model to run, SVC by default
    gridsearch : bool
        Flag for running grid search instead of model, False by default

    Returns
    -------
    accuracy
        an accuracy classification score
    f1score
        a f-measure classification score
    X_train
        pd dataframe of training data
    X_test
        pd dataframe of test data
    '''

    # weights = {'SUS': 1, 'NOT': 3}
    weights = 'balanced'

    models = {
        'SVC' : SVC(kernel = 'linear', probability = True, class_weight = weights, random_state = SEED),
        'LSVC': LinearSVC(class_weight = weights, random_state = SEED),
        'RF': RandomForestClassifier(n_estimators = 100, class_weight = weights, random_state = SEED),
        'NB': MultinomialNB(),
        # 'LGBM': LGBMClassifier(random_state = SEED),
        'LR': LogisticRegression(class_weight = weights, random_state = SEED),
    }

    clf = models[modelname]

    classifier = Pipeline([
        ('features', FeatureUnion(
            transformer_list = [
                ('content_text', Pipeline([
                    ('selector', FunctionTransformer(item_selector, kw_args={'key': 'content'}, validate=False)),
                    # ('word', CountVectorizer(ngram_range = (1, 1))),
                    ('tfidf', TfidfVectorizer(ngram_range = (1, 1))),
                ])),

                # ('content_char', Pipeline([
                #     ('selector', FunctionTransformer(item_selector, kw_args={'key': 'content'}, validate=False)),
                #     ('char', CountVectorizer(analyzer='char', ngram_range=(3,5))),
                #     # ('char', TfidfVectorizer(analyzer='char', ngram_range=(3,7))),
                #
                # ])),

                # ('content_embeds', Pipeline([
                #     ('selector', FunctionTransformer(item_selector, kw_args={'key': 'content'}, validate=False)),
                #     ('word_embeds', FunctionTransformer(get_sent_embedding, kw_args={'word_embeds': embeddings, 'pool': 'average'}, validate=False)),
                # ])),

                ('content_certificates', Pipeline([
                    ('selector', FunctionTransformer(item_selector, kw_args={'key': 'content_raw'}, validate=False)),
                    ('certificates', FunctionTransformer(find_certificates, validate=False)),
                ])),
                #
                # ('brands_name', Pipeline([
                #     ('selector', FunctionTransformer(item_selector, kw_args={'key': 'brand'}, validate=False)),
                #     ('name', FunctionTransformer(check_name, validate=False)),
                # ])),
                #
                # ('url_count', Pipeline([
                #     ('selector', FunctionTransformer(item_selector, kw_args={'key': 'n_url'}, validate=False)),
                #     ('n_urls', FunctionTransformer(count_urls, validate=False)),
                # ])),
            ],
            # weight components in FeatureUnion
            transformer_weights = {
                'content_text': 1.0,
                'content_char': 1.0,
                'content_embeds': 1.0,
                'content_certificates': 1.0,
                'brands_name': 1.0,
                'url_count': 1.0,
            },
        )),
        ('classify', clf)
    ])

    if gridsearch:
        perform_gridsearch(X_train, y_train, classifier, SEED)
        return 0, 0

    else:

        classifier.fit(X_train, y_train)

        # uncomment for printing pipeline parameters
        # print(classifier.named_steps)

        # uncomment to save model to file
        # model_name = 'svc_model.sav'
        # dump(classifier, model_name)

        # uncomment to load model from file
        # classifier = load(model_name)

        y_guess = classifier.predict(X_test)
        y_proba = classifier.predict_proba(X_test)
        y_proba = y_proba[:,1]


        if not y_test.empty:
            ''' predict label for training set '''
            accuracy = accuracy_score(y_test, y_guess)
            f1score = f1_score(y_test, y_guess, average = 'weighted')

            print(confusion_matrix(y_test, y_guess))
            print(classification_report(y_test, y_guess, digits=3))

            return accuracy, f1score, X_train, X_test

        else:
            ''' predict label for hold out set or newly added brands '''
            print('Making decisions..')

            X_test['predict_class'] = y_guess
            X_test['predict_proba'] = y_proba

            X_test = make_decision(X_train, X_test)

            return y_guess, 0, X_train, X_test
