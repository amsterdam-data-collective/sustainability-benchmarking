#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# features for classification

import numpy as np


def find_certificates(X):
    ''' Transformerfunction for counting sustainability certificates in text '''

    certificates = [
        'gots certified',
        'global organic textile standard',
        'oeko tex',
        'bluesign',
        'fair wear foundation',
        'fair trade international',
        'sa8000',
        'ibd organic and brazil organic',
        'world fair trade organization',
        'soil association',
        'b corp',
        'better cotton initiative',
        'peta approved',
        'chetna organic',
        'textile exchange',
        'carbon neutral',
        'ethical trading initiative',
        'fairtrade',
        'ivn',
        'made-by',
        'occguarantee',
        'oeko-tex',
        'peta-approved',
        'scs global services',
        'social enterprise nl',
        'wrap'
    ]

    values = []
    for doc in X:
        doc = [word.lower() for word in doc]
        value = 0
        for c in certificates:
            if c in doc:
                value += 100
        values.append(value)
    return np.array(values).reshape(-1, 1)


def check_name(X):
    ''' Transformerfunction for searching 'green' terms in brandname '''

    greenterms = ['bamboo', 'fair', 'sustainable', 'world', 'ethnic', 'nature', 'good', 'organic']
    values = []
    for doc in X:
        value = 0
        for term in greenterms:
            if term in doc:
                value += 100
        values.append(value)
    return np.array(values).reshape(-1, 1)


def count_urls(X):
    ''' Transformerfunction for counting the amount of urls per brand '''

    return np.array([int(doc) for doc in X]).reshape(-1, 1)


def return_wordcount(X):
    ''' Transformerfunction for counting words per brand '''

    return np.array([len(doc.split()) for doc in X]).reshape(-1, 1)


def return_doclength(X):
    ''' Transformerfunction for measuring document length per brand '''

    return np.array([len(doc) for doc in X]).reshape(-1, 1)


def item_selector(X, key):
    ''' Transformerfunction for selecting column of X '''
    if isinstance(X, str):
        return X
    return X.loc[:, key]


def identity(X):
    ''' Dummy function '''

    return str(X)


def get_sent_embedding(X, word_embeds, pool = 'average'):
    '''Obtains list of embeddings, each representing a whole document'''

    # simply get dim of embeddings
    l_vector = len(word_embeds['a'])

    doc_embedding = []
    for sentence in X:

        # replace each word in sentence with its embedding representation via look up in the embedding dict strcuture
        # if no word_embedding available for a word, just ignore the word
        # [[0.234234,-0.276583...][0.2343, -0.7356354, 0.123 ...][0.2344356, 0.12477...]...]
        list_of_embeddings = [word_embeds[word.lower()] for word in sentence.split() if word.lower() in word_embeds]

        # Obtain sentence embeddings either by average or max pooling on word embeddings of the sentence
        # Option via argument 'pool'
        if pool == 'average':
            sent_embedding = [sum(col) / float(len(col)) for col in zip(*list_of_embeddings)]  # average pooling
        elif pool == 'max':
            sent_embedding = [max(col) for col in zip(*list_of_embeddings)]    # max pooling
        else:
            raise ValueError('Unknown pooling method!')

        # Below case should technically not occur
        if len(sent_embedding) != l_vector:
            sent_embedding = [0] * l_vector

        doc_embedding.append(sent_embedding)

    return doc_embedding
