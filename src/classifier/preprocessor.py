#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for preprocessing the data

import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer, PorterStemmer

def clean_data(X, stem = False, lem = False):
    '''Cleans corpus and tokenizes each document

    Parameters
    ----------
    X : list
        List of strings
    stem : bool, optional
        A flag used to stem words in the corpus (default is False)
    lem : bool, optional
         A flag used to lemmatize words in the corpus (default is False)

    Returns
    -------
    corpus
        a list of strings representing all documents, without stemming/lemmatizing
    corpus_normalized
        a list of strings representing all documents, stemmed/lemmatized if flag is provided
    '''

    print('Starting preprocessing..')

    ignore_keywords = ['javascript', 'browser', 'compatible', 'account', 'cookie', 'cookies', 'policy', 'privacy', 'terms', 'disclaimer', 'warranty', 'ship', 'shipping', 'return', 'returned', 'exchange', 'exchanged', 'please']

    stemmer = PorterStemmer()
    lemmatizer = WordNetLemmatizer()
    corpus = []
    corpus_normalized = []

    for document in X:
        newline_clean = ''
        newline_norm = ''
        lines = document.split('.')
        for line in lines:
            line = re.sub(r'\W', ' ', line)     # remove punctuation
            line = re.sub(r'\s+', ' ', line)    # remove whitespaces
            line = re.sub(' \d+', ' ', line)	# remove numbers
            tok = word_tokenize(line)
            tok = [x.lower() for x in tok]
            if any(word in ignore_keywords for word in tok):	# ignore lines that contain certain keywords
                continue

            tok = [word for word in tok if word not in stopwords.words('english')]

            newline_clean += ' '.join(tok) + ' '

            if stem:
                tok = [stemmer.stem(token) for token in tok]
            if lem:
                tok = [lemmatizer.lemmatize(token, pos = 'v') for token in tok]

            newline_norm += ' '.join(tok) + ' '

        corpus.append(newline_clean)
        corpus_normalized.append(newline_norm)

    print('Done!')
    return corpus, corpus_normalized
