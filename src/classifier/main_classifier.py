#!/usr/bin/python3
# Leon Graumans
# 30-12-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for classification


import json
import sys
import pandas as pd
from joblib import dump, load

from load_data import merge_data
from preprocessor import clean_data
from classify import train_test_analyse
from check import *

sys.path.insert(0, 'scraper')
from export_sql import save_to_sql

def start_classify(brand, brands_dict, html_dict, tmp_brands_dict, tmp_html_dict, current_db, NEW_DB):
    '''Starts classification for new input, overwrites existing json file if we
    decide to post the prediction. Also, new data is inserted into existing SQL table

    Parameters
    ----------
    brand : str
        brand name in string format
    brands_dict : dict
        dictionary containing brands and corresponding values
    html_dict : dict
        dictionary containing IDs and corresponding html pages
    tmp_brands_dict : dict
        emporary dictionary containing new brand
    tmp_html_dict : dict
        temporary dictionary containing IDs and corresponding html pages

    Returns
    -------

    '''

    # create training and test data
    X_train, y_train, _ = merge_data(brands_dict, html_dict)
    X_test, y_test, _ = merge_data(tmp_brands_dict, tmp_html_dict, skiplabel = False)

    # pre-process data
    args_preprocess = False
    if args_preprocess:
        print('Pre-processing test data..')

        corpus_test, corpus_normalized_test = clean_data(X_test['content'], stem = False, lem = True)
        X_test['content_raw'] = corpus_test
        X_test['content'] = corpus_normalized_test

        # choose model
        model_name = 'classifier/saved_models/svc_model_pp_lem.sav'

    else:
        X_train['content_raw'] = X_train['content']
        X_test['content_raw'] = X_test['content']

        # choose model
        model_name = 'classifier/saved_models/svc_model.sav'


    # loading model
    print('Loading model: ', model_name.split('/')[-1])
    classifier = load(model_name)

    # make prediction
    y_guess = classifier.predict(X_test)
    y_proba = classifier.predict_proba(X_test)
    y_proba = y_proba[:,1]

    X_test['predict_class'] = y_guess
    X_test['predict_proba'] = y_proba

    print('Predicted label is: {}  with a probability of {:.2f}.'.format(y_guess, float(y_proba)))

    # check certainty
    X_test = make_decision(X_train, X_test)


    # add predictions for new brand to existing database
    tmp_brands_dict[brand]['predict_class'] = X_test['predict_class']
    tmp_brands_dict[brand]['predict_class'] = X_test[X_test['brand'] == brand]['predict_class'].item()
    tmp_brands_dict[brand]['predict_proba'] = X_test[X_test['brand'] == brand]['predict_proba'].item()
    tmp_brands_dict[brand]['predict_neighbour'] = X_test[X_test['brand'] == brand]['predict_neighbour'].item()
    tmp_brands_dict[brand]['predict_closeclass'] = X_test[X_test['brand'] == brand]['predict_closeclass'].item()
    tmp_brands_dict[brand]['predict_n_std'] = X_test[X_test['brand'] == brand]['predict_n_std'].item()
    tmp_brands_dict[brand]['post_prediction'] = X_test[X_test['brand'] == brand]['post_prediction'].item()


    # open existing json file
    with open('scraper/databases/data.json') as fi:
        json_data = json.load(fi)

    if int(tmp_brands_dict[brand]['post_prediction']):
        print('Writing brand and prediction to json file..')
        json_row = {'brand': brand, 'url': tmp_brands_dict[brand]['url'], 'label': tmp_brands_dict[brand]['predict_class']}
        json_data.append(json_row)

    # overwrite existing json file
    with open('scraper/databases/data.json', 'w') as f:
        json.dump(json_data, f)

    print('Saved new data to existing json file!')

    # insert new data to existing sql database
    if NEW_DB:
        current_db = NEW_DB
    save_to_sql(tmp_brands_dict, tmp_html_dict, current_db, NEW_DB)
