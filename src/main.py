#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# main script for scraping, using user input, and classifying based on a saved model
#
#
# example command:
# python3 main.py scraper/databases/291219_brands_html.db -b adidas -u https://adidas.com

import argparse
import sys

# scrape scripts
sys.path.insert(0, 'scraper')
from main_scraper import start_scraping
from export_sql import save_to_sql

# classify scripts
sys.path.insert(0, 'classifier')
from main_classifier import start_classify
from load_data import load_brands_db, load_html_db


if __name__ == '__main__':

    ''' argparse for handling arguments '''
    parser = argparse.ArgumentParser(description='Duurzame Benchmark scraper task arguments')
    parser.add_argument('database', metavar='[database in .db format]', type=str,
                        help='File containing brands and html tables.')
    parser.add_argument('-b', '--brand', nargs='?', type=str,
                        help='Name of brand that needs to be scraped. Uses hard coded list by default.')
    parser.add_argument('-u', '--url', nargs='?', type=str,
                        help='Homepage of given brand. Uses Google API if url not provided.')
    parser.add_argument('-newdb', '--new_database', nargs='?', type=str,
                        help='Path for creating a new SQL database. Inserts data to current database by default.')
    args = parser.parse_args()

    NEW_DB = args.new_database

    ''' load sql file containing brands and html table, merging them to one dict '''
    current_db = args.database
    # current_db = '../scraper/databases/291219_brands_html.db'
    print('Loading data from file: {}..'.format(current_db.split('/')[-1]))

    brands_dict = load_brands_db(current_db)
    html_dict = load_html_db(current_db)


    if args.brand:
        inputlist = [ (args.brand, args.url) ]
    else:
        # list of tuples with placeholders, used if brand not provided as argument
        inputlist = [ ('<brand>', '<homepage>') ]

    next_idx = 0
    if html_dict:
        # if html_dict exists, continue with an index higher than the max index
        next_idx = max(int(s) for s in html_dict) + 1


    for brand, url in inputlist:

        # if brand already in database and has a homepage, exit
        if brand in brands_dict and brands_dict[brand]['url'] != '':
            print('Brand already in database..')
            exit()

        tmp_brands_dict, tmp_html_dict = start_scraping(brand, url, next_idx, NEW_DB)

        print('Scraping is done, predicting label..')

        start_classify(brand, brands_dict, html_dict, tmp_brands_dict, tmp_html_dict, current_db, NEW_DB)
