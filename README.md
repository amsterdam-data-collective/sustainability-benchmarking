<!-- TITLE -->
# Sustainability Benchmarking :herb:

We assign sustainability ratings to clothing brands, with a little help of Machine Learning and Natural Language Processing.

<img height="256" src="/docs/images/logo.jpg" style="display: block, margin: 0 auto">

<!-- ABOUT THE PROJECT -->
## About The Project

The global fashion industry has a huge problem with working environments, waste and human right abuses. This is due to the pressure to complete orders on time, which is caused by short production deadlines and by the quick change of designs, referred to as _fast fashion_. One solution to solve them all is not yet available. But, if you wear clothes, you are part of the solution, by buying sustainable clothing instead.

Many great initiatives are pushing in the right direction by selling clothings with a sustainability label. But, from the thousands of clothing brands available, how do you decide which brand is best? Wouldn’t it be great to have one website you can consult to find one, reliable answer?

That’s where we come in. Our model is trained to predict sustainability ratings for as much clothing brands as possible, which are saved in a database for the public to see. Of course, clothing has an impact on a variety of areas, such as environment, human rights and recycling. We could discuss for many years about an exact definition of sustainability or more generally speaking ‘wrong and right’. We do not strive for an unbreakable solution (yet), but rather try to improve the situation step by step.

This repository contains the source code of our Sustainability Benchmarking project. We use scraping to gather useful information about each clothing brand, which we subsequently use to train our AI model. More data means a better model, and a better model means a better sustainability benchmark. A more detailed explanation of this project can be found in the [Documentation](DOC.md) file.

_Quality of the source and the ability to adjust your website to score higher is one big caveat we are aware of. We encourage every brand to become more transparent and provide more information. The model will be dynamic and adjusted accordingly. Moreover, we believe that if brands bluntly provide wrong information on their websites that the press will (eventually) find out._

If you are interested in receiving updates about this topic or want to talk more about the combination between Sustainability and AI, please [contact](#contact) us.


### Built With

* [Python 3](https://www.python.org/)
* [BeautifulSoup4](https://www.crummy.com/software/BeautifulSoup/)
* [NLTK](https://www.nltk.org/)
* [scikit-learn](https://scikit-learn.org/stable/)


### Room for Improvement
We would love it if you would contribute to this project. Hereby some inspiration for future development.

* Improve usage of different languages, other than English -> Currently, we translated non-English content with the Google API ([translators](https://github.com/uliontse/translators)).   
* Extract more features from scraped material, like price categories, country of origin, external links etc.
* Extract pdf files from website, as these files possibly contain information about sustainability.
* Include more data sources, like news websites, external databases etc.
* Try other models (more advanced Neural Networks, BERT, etc.)
* Information/summary about each brand (text summarization)




<!-- INSTALLATION -->
## Installation

### Clone

Clone this repo to your local machine using `git clone https://gitlab.com/amsterdam-data-collective/sustainability-benchmarking.git`

### Installing

```shell
$ pip install -r requirements.txt
```

<!-- USAGE -->
## Usage

The argument usage of this script is as follows. Providing a database in .db-format is required.
If you want to create a new database file, provide the `-newdb` argument, followed by the path for the new database. By default, new data is inserted into the existing database.

`python3 main.py [-h] [-b [BRAND]] [-u [URL]] [-newdb] [database in .db format]`

For passing a brand name and, optionally, a homepage, run the following command:

`python3 main.py [database in .db format] --brand [brandname] --url [hyperlink]`

Run the following to print a detailed argument usage:

`python3 main.py --help`


## Important Files

For this project we make use of a self-created SQL database. Please [contact](#contact) us if you're interested in receiving a copy of this database.

Also, we use pre-trained Word2Vec embeddings. To download, please use the following command:  
`wget -c "https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz"`



<!-- FILE STRUCTURE -->
## File structure

    .
    │        
    ├── src                         <- Directory containing all source code
    │   │                            
    │   ├── main.py
    │   │                           
    │   ├── scraper                 <- Scripts to scrape data for new clothing brands and inserting
    │   │   │                          this data to the existing database.
    │   │   ├── databases
    │   │   │    ├── data.json
    │   │   │    ├── ietf-language-tags_csv.csv
    │   │   │    └── page_dictionary.txt
    |   |   |
    │   │   ├── main.py                    
    │   │   ├── export_sql.py            
    │   │   ├── retrieve_content.py        
    │   │   ├── retrieve_homepage.py
    │   │   ├── url_picker.py            
    │   │   └── util.py
    │   │            
    │   └── classifier              <- Scripts to train models and then use trained models to make
    │       │                          predictions.
    │       ├── saved_models
    │       │   ├── svc_model_pp_em.sav
    │       │   └── svc_model.sav
    |       |
    │       ├── main.py
    │       ├── check.py                    
    │       ├── classify.py                
    │       ├── features.py                
    │       ├── labels.py                
    │       ├── load_data.py            
    │       └── preprocessor.py
    │
    ├── benchmark_env.yml           <- YAML file containing conda environment.
    ├── DOC.md                      <- DOC file containing documentation.
    ├── LICENSE.md                  <- LICENSE file containing permissions and limitations.
    ├── README.md                   <- The top-level README for developers using this project.
    └── requirements.txt            <- The requirements file for reproducing the analysis environment,
                                       e.g. generated with `pip freeze > requirements.txt`


<!-- LICENSE -->
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.



<!-- CONTACT -->
## Contact

Joanneke Meijer - [LinkedIN](https://www.linkedin.com/in/joannekemeijer/) -  [jmeijer@amsterdamdatacollective.com](jmeijer@amsterdamdatacollective.com)  
Leon Graumans - [LinkedIN](https://www.linkedin.com/in/leongraumans/)

Project Link: [https://gitlab.com/amsterdam-data-collective/sustainability-benchmarking](https://gitlab.com/amsterdam-data-collective/sustainability-benchmarking)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

### Projects regarding sustainable clothing
* [ProjectCece](https://www.projectcece.nl/)


### Libraries
* [ELI5](https://github.com/TeamHG-Memex/eli5)
* [Gensim](https://radimrehurek.com/gensim/)
* [googlesearch Library](https://pypi.org/project/google/)
* [Langdetect](https://github.com/Mimino666/langdetect)
* [LightGBM](https://github.com/microsoft/LightGBM)
* [LIME](https://github.com/marcotcr/lime)
* [Matplotlib](https://matplotlib.org/)
* [Numpy](https://numpy.org/)
* [Pandas](https://pandas.pydata.org/)
* [Requests](https://2.python-requests.org/en/master/)
* [translators](https://github.com/uliontse/translators)
* [word_cloud](https://github.com/amueller/word_cloud)
